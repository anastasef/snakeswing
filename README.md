# Simple snake game

## How to play

1. Launch the main function
2. Snake controls: WASD

## Features:

- Score counting
- Best score
- Snake dies if it hits the playground boundaries and it's tail

### To do

Gui and functionality of:
- starting the game,
- pausing it,
- and quitting

## Tools used

- Java 11
- Swing & awt

## References

- [Video tutorial](https://www.youtube.com/watch?v=OZYVfVxB81s)