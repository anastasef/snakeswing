package nast.game;

/**
 * Snake's head<br>
 *     Holds it's coordinates (in squares) and it's direction - up / left / down / right.
 */
public class Head {

    Direction dir = Direction.RIGHT; // snake's head's direction
    int x, y;

    public Head(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Direction getDir() {
        return dir;
    }

    public void setDir(Direction dir) {
        this.dir = dir;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
