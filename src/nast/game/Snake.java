package nast.game;

import nast.gui.Gui;

import java.awt.*;
import java.util.ArrayList;

/**
 * The main class of the snake
 */
public class Snake {

    public static boolean waitToMove = false; // true if the snake is processing a move
    // Place the head in the middle of the grid
    public static Head head = new Head((Gui.GRID_SQUARE_COUNT / 2), (Gui.GRID_SQUARE_COUNT / 2));
    public static ArrayList<Tail> tails = new ArrayList<>(); // list of tail parts
    public static PickUp pickUp = new PickUp();

    public static void addTail() {
        if (tails.size() < 1) {
            tails.add(new Tail(head.getX(), head.getY()));
        } else {
            tails.add(new Tail(
                    tails.get(tails.size() - 1).x,
                    tails.get(tails.size() - 1).y)
            );
        }
    }

    /**
     * Move the snake from the last tail to the head:<br>
     *     1. Move the tail from the last part to the second (inclusive).<br>
     *     2. Move the first part of the tail - it takes the head's position.<br>
     *     3. Move the head according to it's current direction.
     */
    public static void move() {
        // Move tails, except the first tail after the head
        if (tails.size() >= 2) {
            for (int i = tails.size() - 1; i >= 1; i--) {
                if (tails.get(i).isWaiting()) {     // The tail is already moving
                    tails.get(i).setWaiting(false); // Do not change coords during current game clock,
                                                    // leave it for the next one
                } else {
                    // Change coords in current game clock
                    // Coords of the current tail = coords of the previous (the snake is moving always forward)
                    tails.get(i).setX(tails.get(i - 1).getX());
                    tails.get(i).setY(tails.get(i - 1).getY());
                }
            }
        }
        // Move the first tail after the head
        if (tails.size() >= 1) {
            if (tails.get(0).isWaiting()) {     // The tail is already moving
                tails.get(0).setWaiting(false); // Do not change coords during current game clock,
                                                // leave it for the next one
            } else {
                // Change coords in current game clock
                // Coords of the current tail = coords of the previous (the snake is moving always forward)
                tails.get(0).setX(head.getX());
                tails.get(0).setY(head.getY());
            }
        }

        // Move the head
        switch (head.getDir()) {
            case RIGHT:
                head.setX(head.getX() + 1);
                break;
            case UP:
                head.setY(head.getY() - 1);
                break;
            case LEFT:
                head.setX(head.getX() - 1);
                break;
            case DOWN:
                head.setY(head.getY() + 1);
                break;
        }
    }

    /**
     * Transforms a position measured in squares to it's coordinates in pixels (top left corner).
     * @param x position nth square horizontally
     * @param y position nth square vertically
     * @return (x, y) position in pixels (top left corner)
     */
    public static Point ptc(int x, int y) {
        Point p = new Point(0, 0);

        p.x = x * Gui.GRID_SQUARE_SIDE + Gui.X_OFFSET;
        p.y = y * Gui.GRID_SQUARE_SIDE + Gui.Y_OFFSET;

        return p;
    }
}
