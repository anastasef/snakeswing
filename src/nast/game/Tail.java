package nast.game;

/**
 * Snake's tail segment:<br>
 *     Holds it's coordinates (in squares) and can wait for the head state.
 */
public class Tail {
    int x, y;            // Position in squares ?? TODO
    boolean waiting = true; // Wait for the head

    public Tail(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    /**
     * If the tail part is waiting for the head to move.<br>
     * If true: do not change coords during current game clock.
     * @return
     */
    public boolean isWaiting() {
        return waiting;
    }

    public void setWaiting(boolean waiting) {
        this.waiting = waiting;
    }

}
