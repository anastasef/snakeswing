package nast.game;

import nast.gui.Gui;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Snake's food
 */
public class PickUp {

    int x, y;

    public PickUp() {
        reset();
    }

    public void reset() {
        this.x = ThreadLocalRandom.current().nextInt(0, Gui.GRID_SQUARE_COUNT);
        this.y = ThreadLocalRandom.current().nextInt(0, Gui.GRID_SQUARE_COUNT);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
