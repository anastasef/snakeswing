package nast.game;

/**
 * Directions: up, left, right, down
 */
public enum Direction {
    UP, LEFT, RIGHT, DOWN
}
