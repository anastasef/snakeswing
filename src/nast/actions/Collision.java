package nast.actions;

import nast.game.Game;
import nast.game.Snake;
import nast.game.Tail;
import nast.gui.Gui;

public class Collision {

    /**
     * Is colliding with itself? (example: snake colliding its own tail)
     * @return
     */
    public static boolean collideSelf() {

        for (Tail tail : Snake.tails) {
            if (Snake.head.getX() == tail.getX()
                    && Snake.head.getY() == tail.getY()
                    && !tail.isWaiting()) {
                return true;
            }
        }
        return false;
    }

    public static boolean collideWall() {
        return (Snake.head.getX() < 0 || Snake.head.getX() >= Gui.GRID_SQUARE_COUNT ||
                Snake.head.getY() < 0 || Snake.head.getY() >= Gui.GRID_SQUARE_COUNT);
    }

    /**
     * If snake's head is colliding with the pickup - then add the tail.
     */
    public static void collidePickUp() {
        if (Snake.head.getX() == Snake.pickUp.getX() && Snake.head.getY() == Snake.pickUp.getY()) {
            Snake.pickUp.reset();
            Snake.addTail();
            Game.score++;
            if (Game.score > Game.bestScore) {
                Game.bestScore = Game.score;
            }
        }
    }
}
