package nast.actions;

import nast.game.Direction;
import nast.game.Snake;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Handle WASD key pressings.
 * A WASD key changes the snake's head direction
 */
public class KeyHandler implements KeyListener {

    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        switch (keyEvent.getKeyCode()) {
            case KeyEvent.VK_W:
                if (!(Snake.head.getDir() == Direction.DOWN) && !Snake.waitToMove) {
                    Snake.head.setDir(Direction.UP);
                    Snake.waitToMove = true; // Blocks other move actions while the clocks thread sleeps
                }
                break;
            case KeyEvent.VK_A:
                if (!(Snake.head.getDir() == Direction.RIGHT) && !Snake.waitToMove) {
                    Snake.head.setDir(Direction.LEFT);
                    Snake.waitToMove = true; // Blocks other move actions while the clocks thread sleeps
                }
                break;
            case KeyEvent.VK_S:
                if (!(Snake.head.getDir() == Direction.UP) && !Snake.waitToMove) {
                    Snake.head.setDir(Direction.DOWN);
                    Snake.waitToMove = true; // Blocks other move actions while the clocks thread sleeps
                }
                break;
            case KeyEvent.VK_D:
                if (!(Snake.head.getDir() == Direction.LEFT) && !Snake.waitToMove) {
                    Snake.head.setDir(Direction.RIGHT);
                    Snake.waitToMove = true; // Blocks other move actions while the clocks thread sleeps
                }
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {

    }
}
