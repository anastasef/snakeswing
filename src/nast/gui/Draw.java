package nast.gui;

import nast.game.Game;
import nast.game.Snake;

import javax.swing.*;
import java.awt.*;

/**
 * Scene rendering:<br>
 * background, snake tails, grid, game's area grid border.
 */
public class Draw extends JLabel {

    Point p;

    /**
     * Renders the scene: background, snake tails, grid, game's area grid border.
     * <br>
     * Who calls paintComponent?:
     * <br>
     * - When a window becomes visible (uncovered or deminimized) or is resized, the "system" automatically
     * calls the paintComponent() method for all areas of the screen that have to be redrawn.
     * <br>
     * - Indirectly via repaint() call.
     * @param g drawing context
     */
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);

        // Draw background
        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(0, 0, Gui.WINDOW_WIDTH, Gui.WINDOW_HEIGHT);

        // Draw pickup
        g.setColor(new Color(234, 0, 67));
        p = Snake.ptc(Snake.pickUp.getX(), Snake.pickUp.getY());
        g.fillOval(p.x, p.y, Gui.GRID_SQUARE_SIDE, Gui.GRID_SQUARE_SIDE);

        // Draw snake's tails
        g.setColor(new Color(154, 213, 58));
        for (int i = 0; i < Snake.tails.size(); i++) {
            p = Snake.ptc(Snake.tails.get(i).getX(), Snake.tails.get(i).getY());
            g.fillRect(p.x, p.y, Gui.GRID_SQUARE_SIDE, Gui.GRID_SQUARE_SIDE);
        }

        // Draw snake's head
        g.setColor(new Color(0, 149, 48));
        p = Snake.ptc(Snake.head.getX(), Snake.head.getY());
        g.fillRect(p.x, p.y, Gui.GRID_SQUARE_SIDE, Gui.GRID_SQUARE_SIDE);

        // Draw grid
        g.setColor(Color.GRAY);
        for (int i = 0; i < Gui.GRID_SQUARE_COUNT; i++) {
            for (int j = 0; j < Gui.GRID_SQUARE_COUNT; j++) {
                g.drawRect(i * Gui.GRID_SQUARE_SIDE + Gui.X_OFFSET,
                        j * Gui.GRID_SQUARE_SIDE + Gui.Y_OFFSET,
                        Gui.GRID_SQUARE_SIDE, Gui.GRID_SQUARE_SIDE);
            }
        }

        // Draw border
        g.setColor(Color.BLACK);
        g.drawRect(Gui.X_OFFSET, Gui.Y_OFFSET,
                Gui.GRID_SQUARE_SIDE * Gui.GRID_SQUARE_COUNT, Gui.GRID_SQUARE_SIDE * Gui.GRID_SQUARE_COUNT);

        // Draw score
        g.setFont(new Font("Arial", Font.BOLD, 20));
        g.drawString("Score: " + Game.score,
                (Gui.GRID_SQUARE_SIDE * Gui.GRID_SQUARE_COUNT + Gui.X_OFFSET * 2),
                Gui.Y_OFFSET);
        g.drawString("Best score: " + Game.bestScore,
                (Gui.GRID_SQUARE_SIDE * Gui.GRID_SQUARE_COUNT + Gui.X_OFFSET * 2),
                Gui.Y_OFFSET + Gui.GRID_SQUARE_SIDE);

        repaint();
    }
}
