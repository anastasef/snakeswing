package nast.gui;

import nast.actions.KeyHandler;

import javax.swing.*;

/**
 * Sets up the JFrame;<br>
 * Holds drawn elements' sizes (window size, grid squares size, window paddings = offsets).
 */
public class Gui {

    JFrame jf;
    Draw d;

    public static final int WINDOW_WIDTH = 800, WINDOW_HEIGHT = 600;
    public static final int X_OFFSET = 20, Y_OFFSET = 20; // Window padding
    public static final int GRID_SQUARE_SIDE = 32;
    public static final int GRID_SQUARE_COUNT = 16;

    /**
     * Set up the main window (JFrame) and instantiate the Draw class (JLabel),
     * which renders the scene with all its elements.
     */
    public void create() {
        jf = new JFrame("snake");
        jf.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jf.setLocationRelativeTo(null);
        jf.setLayout(null);
        jf.setResizable(false);
        jf.addKeyListener(new KeyHandler());

        d = new Draw();
        d.setBounds(0,0, WINDOW_WIDTH, WINDOW_HEIGHT);
        d.setVisible(true);
        jf.add(d);

        jf.requestFocus();
        jf.setVisible(true);

    }
}
