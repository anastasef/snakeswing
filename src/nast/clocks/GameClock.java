package nast.clocks;

import nast.actions.Collision;
import nast.game.Game;
import nast.game.Snake;
import nast.gui.Gui;

public class GameClock extends Thread {

    public static boolean running = true; // dead or not

    public void run() {
        while(running) {
            try {
                sleep(200);
                Snake.move();
                Snake.waitToMove = false; // true if the snake is processing a move

                Collision.collidePickUp();

                if (Collision.collideSelf()) {
                    Snake.tails.clear();
                    // game over
                    Game.resetScore();
                }

                if (Collision.collideWall()) {
                    Snake.tails.clear();
                    // Place the head in the middle of the grid
                    Snake.head.setX(Gui.GRID_SQUARE_COUNT / 2);
                    Snake.head.setY(Gui.GRID_SQUARE_COUNT / 2);
                    // game over
                    Game.resetScore();
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
